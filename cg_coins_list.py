from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json
from datetime import datetime
from prettytable import PrettyTable


api_url = 'https://api.coingecko.com/api/v3/'
api_fun = '/coins/list'
url = api_url+api_fun


parameters = {
}

headers = {
}


session = Session()
session.headers.update(headers)


try:
  response = session.get(url, params=parameters)
  data = json.loads(response.text)
except (ConnectionError, Timeout, TooManyRedirects) as e:
  print(e)


coins_table = PrettyTable()
coins_table.field_names = ["id", "name", "symbol"]


for coin in data:
  coin_id = coin['id']
  coin_name = coin['name']
  coin_symbol = coin['symbol']

  coins_table.add_row([
      coin_id,
      coin_name,
      coin_symbol
  ])


print(coins_table)