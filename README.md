# CoinGecko Portfolio
Script pulls data using CoinGecko API and provides information about current prices and value of the portfolio.


This tiny project contains following files:
 - **cg_portfolio.py** - script which displays crytpo portfolio
 - **cg_portfolio.json** - example file with coin portfolio (pre-populated with some data)
 - **cg_coin_list.txt** - list of coins from CoinGecko API (keep in mind list might be outdated)
 - **cg_coin_list.py** - script used to create cg_coin_list.txt. Can be used to refresh list of coins.


## cd_portfolio.py
Script pulls data from CoinGecko.
By default script is looking for cg_portfolio.json in same folder where script is located.
If portfolio file has different name it can be passed to script as a parameter.
That allows to keep different sets of crypto portfolio in different files and call script passing file name during execution.
```bash
cd_portfolio.py -f portfolio_file.json
```


## cg_portfolio.json
File contains crypto asset portfolio.
Data with portfolio entries have to go to "portfolio" section of the file.
Example entries:
```json
  "portfolio": [
    {
      "id": "algorand",
      "amount": 12
    },
    {
      "id": "litecoin",
      "amount": 10
    },

...
```
If you want to keep portfolio in file with different name, just copy or rename cg_portfolio.json and modify it.


## cg_coin_list.txt
File contains list of all crypto assets available via CoinGecko API.
That list might be outdated.
To pull most recent list of crypto assets use cg_coin_list.py script.
Column id from the file should be used in portfolio file.


## License
MIT / BSD


## Donations
If you found some value and would like ot support this and future projects you can donate:

**BTC** 3LX9cdyXvbAdhUnCteKVtS7uxQz2mqeLfB  
**LTC** MHKgdcZxcyC937otEqwMWvSPANmRVXdbyC  
**ETH** 0xe2D5c108C8219f97d7a3a91558ff11138eEd2814  
**DGB** DJgNTXkyG5u6rujVMZFQDPtf7ueSgAomNw  
**BCH** qz4lm4fxn8tvrze33afr85sfrz73uz5vwcycyngphk  
